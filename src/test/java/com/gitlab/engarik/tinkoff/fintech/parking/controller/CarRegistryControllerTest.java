package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.model.CarRegistry;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRegistryRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRegistryRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = CarRegistryControllerTest.class)
class CarRegistryControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final UUID testCarId = UUID.randomUUID();
    private final UUID testCarId2 = UUID.randomUUID();
    private final UUID testEmployeeId = UUID.randomUUID();
    @Autowired
    CarRegistryRepository carRegistryRepository;
    @Autowired
    CarRepository carRepository;
    @Autowired
    EmployeeRepository employeeRepository;
    @Autowired
    ObjectMapper jackson;
    @Autowired
    private MockMvc mvc;
    @Value("${custom.base-url}/cars-registry")
    private String URI;

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @BeforeEach
    public void init() {
        carRepository.save(Car.builder()
                .carId(testCarId)
                .model("Tesla")
                .registrationPlate("A000AA178")
                .build());

        carRepository.save(Car.builder()
                .carId(testCarId2)
                .model("Nissan")
                .registrationPlate("A111AA97")
                .build());

        employeeRepository.save(Employee.builder()
                .name("Nikita")
                .employeeId(testEmployeeId)
                .build());
    }

    @AfterEach
    public void clear() {
        carRegistryRepository.deleteAll();
        carRepository.deleteAll();
        employeeRepository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        CarRegistryRequest carRegistryRequest = createCarRegistryRequest(testEmployeeId, testCarId);

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(carRegistryRequest)))
                .andExpect(status().isCreated());
    }

    @Test
    void findSuccess() throws Exception {
        carRegistryRepository.save(testEmployeeId, testCarId);

        mvc.perform(get(URI + "/{employeeId}/{carId}", testEmployeeId, testCarId))
                .andExpect(status().isOk());
    }

    @Test
    void findIdNotFound() throws Exception {
        mvc.perform(get(URI + "/{employeeId}/{carId}", UUID.randomUUID(), UUID.randomUUID()))
                .andExpect(content().string("false"))
                .andExpect(status().isOk());
    }

    @Test
    void findAll() throws Exception {
        CarRegistryRequest carRegistryRequest1 = createDefaultCarRegistryRequest();
        CarRegistryRequest carRegistryRequest2 = createCarRegistryRequestAndSaveToDatabase(testEmployeeId, testCarId2);

        mvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(carRegistryRequest1, carRegistryRequest2))));

    }

    @Test
    void deleteSuccess() throws Exception {
        CarRegistryRequest carRegistryRequest = createDefaultCarRegistryRequest();

        mvc.perform(
                        delete(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(carRegistryRequest)))
                .andExpect(status().isOk());
    }

    @Test
    void deleteIdNotFound() throws Exception {
        CarRegistryRequest carRegistryRequest = new CarRegistryRequest(UUID.randomUUID(), UUID.randomUUID());
        mvc.perform(
                        delete(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(carRegistryRequest)))
                .andExpect(status().isNotFound());
    }

    private CarRegistryRequest createCarRegistryRequest(UUID employeeId, UUID carId) {
        CarRegistryRequest carRegistryRequest = new CarRegistryRequest();

        carRegistryRequest.setEmployeeId(employeeId);
        carRegistryRequest.setCarId(carId);

        return carRegistryRequest;
    }

    private CarRegistryRequest createCarRegistryRequestAndSaveToDatabase(UUID employeeId, UUID carId) {
        CarRegistryRequest carRegistryRequest = createCarRegistryRequest(employeeId, carId);

        carRegistryRepository.save(employeeId, carId);

        return carRegistryRequest;
    }

    private CarRegistryRequest createDefaultCarRegistryRequest() {
        return createCarRegistryRequestAndSaveToDatabase(testEmployeeId, testCarId);
    }

}