package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = ParkingControllerTest.class)
class ParkingControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    ObjectMapper jackson;

    @Autowired
    private MockMvc mvc;

    @Value("${custom.base-url}/parking")
    private String URI;

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @AfterEach
    public void clear() {
        parkingRepository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        Parking parking = createDefaultParking();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(parking)))
                .andExpect(status().isCreated());

    }

    @Test
    void findSuccess() throws Exception {
        Parking parking = createParkingAndSaveToDatabase("Saint-Petersburg");

        mvc.perform(get(URI + "/{parkingId}", parking.getParkingId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parking)));

    }

    @Test
    void findIdNotFound() throws Exception {
        mvc.perform(get(URI + "/{parkingId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll() throws Exception {
        Parking parking1 = createParkingAndSaveToDatabase("Saint-Petersburg");
        Parking parking2 = createParkingAndSaveToDatabase("Moscow");

        mvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(parking1, parking2))));

    }

    @Test
    void updateSuccess() throws Exception {
        Parking parking = createParkingAndSaveToDatabase("Saint-Petersburg");

        mvc.perform(get(URI + "/{parkingId}", parking.getParkingId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parking)));

        parking.setAddress("Nizhny Novgorod");

        mvc.perform(
                        put(URI + "/{parkingId}", parking.getParkingId())
                                .content(jackson.writeValueAsString(parking))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get(URI + "/{parkingId}", parking.getParkingId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parking)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        Parking parking = createDefaultParking();

        mvc.perform(
                        put(URI + "/{parkingId}", parking.getParkingId())
                                .content(jackson.writeValueAsString(parking))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        Parking parking = createParkingAndSaveToDatabase("Saint-Petersburg");

        mvc.perform(delete(URI + "/{parkingId}", parking.getParkingId()))
                .andExpect(status().isOk());

    }

    @Test
    void deleteIdNotFound() throws Exception {
        mvc.perform(delete(URI + "/{parkingId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    private Parking createDefaultParking() {
        return Parking.builder()
                .address("Saint-Petersburg")
                .build();
    }

    private Parking createParkingAndSaveToDatabase(String city) {
        Parking parking = Parking.builder()
                .address(city)
                .build();

        parkingRepository.save(parking);

        return parking;
    }

}