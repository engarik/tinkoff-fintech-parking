package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = CarControllerTest.class)
class CarControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @Autowired
    CarRepository carRepository;

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper jackson;

    @Value("${custom.base-url}/cars")
    private String URI;

    @AfterEach
    public void clear() {
        carRepository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        Car car = createDefaultCar();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(car)))
                .andExpect(status().isCreated());

    }

    @Test
    void findSuccess() throws Exception {
        Car car = createCarAndSaveToDatabase("Tesla", "RU");

        mvc.perform(get(URI + "/{carId}", car.getCarId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(car)));

    }

    @Test
    void findIdNotFound() throws Exception {
        mvc.perform(get(URI + "/{carId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll() throws Exception {
        Car car1 = createCarAndSaveToDatabase("Tesla", "RU");
        Car car2 = createCarAndSaveToDatabase("Lada", "RU");

        mvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(car1, car2))));

    }

    @Test
    void updateSuccess() throws Exception {
        Car car = createCarAndSaveToDatabase("Tesla", "RU");

        mvc.perform(get(URI + "/{carId}", car.getCarId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(car)));

        car.setModel("Nissan");
        car.setRegistrationPlate("EN");

        mvc.perform(
                        put(URI + "/{carId}", car.getCarId())
                                .content(jackson.writeValueAsString(car))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get(URI + "/{carId}", car.getCarId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(car)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        Car car = createDefaultCar();

        mvc.perform(
                        put(URI + "/{carId}", car.getCarId())
                                .content(jackson.writeValueAsString(car))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        Car car = createCarAndSaveToDatabase("Tesla", "RU");

        mvc.perform(delete(URI + "/{carId}", car.getCarId()))
                .andExpect(status().isOk());

    }

    @Test
    void deleteIdNotFound() throws Exception {
        mvc.perform(delete(URI + "/{carId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    private Car createDefaultCar() {
        return Car.builder()
                .model("Tesla")
                .registrationPlate("RU")
                .build();
    }

    private Car createCarAndSaveToDatabase(String model, String registrationPlate) {
        Car car = Car.builder()
                .model(model)
                .registrationPlate(registrationPlate)
                .build();

        carRepository.save(car);

        return car;
    }

}