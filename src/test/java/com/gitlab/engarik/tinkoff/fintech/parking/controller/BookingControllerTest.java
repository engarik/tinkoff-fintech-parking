package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.*;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@ActiveProfiles("test")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = BookingControllerTest.class)
class BookingControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Autowired
    BookingRepository bookingRepository;

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    CarRepository carRepository;

    @Autowired
    CarRegistryRepository carRegistryRepository;

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    ParkingLotRepository parkingLotRepository;
    @Autowired
    ObjectMapper jackson;
    @Autowired
    private MockMvc mvc;
    @Value("${custom.base-url}/booking")
    private String URI;

    private UUID employeeId0 = UUID.randomUUID();
    private UUID employeeId1 = UUID.randomUUID();
    private UUID carId0 = UUID.randomUUID();
    private UUID carId1 = UUID.randomUUID();
    private UUID parkingId0 = UUID.randomUUID();
    private UUID parkingId1 = UUID.randomUUID();
    private UUID parkingLotId0 = UUID.randomUUID();
    private UUID parkingLotId1 = UUID.randomUUID();
    private UUID parkingLotId2 = UUID.randomUUID();
    private UUID parkingLotId3 = UUID.randomUUID();

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @AfterEach
    public void clear() {
        employeeRepository.deleteAll();
        carRepository.deleteAll();
        carRegistryRepository.deleteAll();
        parkingLotRepository.deleteAll();
        parkingRepository.deleteAll();
        bookingRepository.deleteAll();
    }

    @BeforeEach
    public void init() {
        initEmployees();
        initCars();
        initCarRegistry();
        initParking();
        initParkingLots();
    }

    @Test
    void saveSuccess() throws Exception {
        Booking booking = Booking.builder()
                .startTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0)))
                .endTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(19, 0)))
                .employeeId(employeeId0)
                .carId(carId0)
                .parkingLotId(parkingLotId0)
                .build();

        mvc.perform(post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(booking)))
                .andExpect(status().isCreated());

    }

    @Test
    void saveFailCarAlreadyParked() throws Exception {
        bookingRepository.save(Booking.builder()
                .startTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0)))
                .endTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(15, 0)))
                .employeeId(employeeId0)
                .carId(carId0)
                .parkingLotId(parkingLotId1)
                .build());

        Booking booking = Booking.builder()
                .startTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0)))
                .endTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(19, 0)))
                .employeeId(employeeId0)
                .carId(carId0)
                .parkingLotId(parkingLotId0)
                .build();

        mvc.perform(post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(booking)))
                .andExpect(status().is4xxClientError());

    }

    @Test
    void saveFailParkingLotIsOccupied() throws Exception {
        bookingRepository.save(Booking.builder()
                .startTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0)))
                .endTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(15, 0)))
                .employeeId(employeeId0)
                .carId(carId0)
                .parkingLotId(parkingLotId0)
                .build());

        Booking booking = Booking.builder()
                .startTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(10, 0)))
                .endTime(LocalDateTime.of(LocalDate.now(), LocalTime.of(19, 0)))
                .employeeId(employeeId1)
                .carId(carId1)
                .parkingLotId(parkingLotId0)
                .build();

        mvc.perform(post(URI)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(jackson.writeValueAsString(booking)))
                .andExpect(status().is4xxClientError());

    }

    private void initParkingLots() {
        parkingLotRepository.save(ParkingLot.builder()
                .parkingLotId(parkingLotId0)
                .parkingId(parkingId0)
                .position("1")
                .build());
        parkingLotRepository.save(ParkingLot.builder()
                .parkingLotId(parkingLotId1)
                .parkingId(parkingId0)
                .position("2")
                .build());
        parkingLotRepository.save(ParkingLot.builder()
                .parkingLotId(parkingLotId2)
                .parkingId(parkingId1)
                .position("56")
                .build());
        parkingLotRepository.save(ParkingLot.builder()
                .parkingLotId(parkingLotId3)
                .parkingId(parkingId1)
                .position("80")
                .build());
    }

    private void initParking() {
        parkingRepository.save(Parking.builder()
                .parkingId(parkingId0)
                .address("Khersonskaya Ulitsa, 12-14, St Petersburg, 191024")
                .build());

        parkingRepository.save(Parking.builder()
                .parkingId(parkingId1)
                .address("Alekseevskaya St, 10/16, Nizhny Novgorod, Nizhny Novgorod Oblast, 603005")
                .build());
    }

    private void initEmployees() {
        employeeRepository.save(Employee.builder()
                .employeeId(employeeId0)
                .name("Ivan Ivanov")
                .build());

        employeeRepository.save(Employee.builder()
                .employeeId(employeeId1)
                .name("Anna Pavlova")
                .build());
    }

    private void initCars() {
        carRepository.save(Car.builder()
                .carId(carId0)
                .model("Tesla")
                .registrationPlate("A000AA178")
                .build());

        carRepository.save(Car.builder()
                .carId(carId1)
                .model("Ford")
                .registrationPlate("A222BB178")
                .build());
    }

    private void initCarRegistry() {
        carRegistryRepository.save(employeeId0, carId0);
        carRegistryRepository.save(employeeId0, carId1);
        carRegistryRepository.save(employeeId1, carId1);
    }

}