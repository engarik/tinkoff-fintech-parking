package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.EmployeeRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = EmployeeControllerTest.class)
class EmployeeControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @Autowired
    EmployeeRepository employeeRepository;

    @Autowired
    private MockMvc mvc;

    @Autowired
    ObjectMapper jackson;

    @Value("${custom.base-url}/employees")
    private String URI;

    @AfterEach
    public void clear() {
        employeeRepository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        Employee employee = createDefaultEmployee();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(employee)))
                .andExpect(status().isCreated());

    }

    @Test
    void findSuccess() throws Exception {
        Employee employee = createEmployeeAndSaveToDatabase("Employee");

        mvc.perform(get(URI + "/{employeeId}", employee.getEmployeeId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(employee)));

    }

    @Test
    void findIdNotFound() throws Exception {
        mvc.perform(get(URI + "/{employeeId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll() throws Exception {
        Employee employee1 = createEmployeeAndSaveToDatabase("Employee1");
        Employee employee2 = createEmployeeAndSaveToDatabase("Employee2");

        mvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(employee1, employee2))));

    }

    @Test
    void updateSuccess() throws Exception {
        Employee employee = createEmployeeAndSaveToDatabase("Employee");

        mvc.perform(get(URI + "/{employeeId}", employee.getEmployeeId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(employee)));

        employee.setName("EmployeeUpdated");

        mvc.perform(
                        put(URI + "/{employeeId}", employee.getEmployeeId())
                                .content(jackson.writeValueAsString(employee))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get(URI + "/{employeeId}", employee.getEmployeeId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(employee)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        Employee employee = createDefaultEmployee();

        mvc.perform(
                        put(URI + "/{employeeId}", employee.getEmployeeId())
                                .content(jackson.writeValueAsString(employee))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        Employee employee = createEmployeeAndSaveToDatabase("Employee");

        mvc.perform(delete(URI + "/{employeeId}", employee.getEmployeeId()))
                .andExpect(status().isOk());

    }

    @Test
    void deleteIdNotFound() throws Exception {
        mvc.perform(delete(URI + "/{employeeId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    private Employee createDefaultEmployee() {
        return Employee.builder()
                .name("Employee")
                .build();
    }

    private Employee createEmployeeAndSaveToDatabase(String name) {
        Employee employee = Employee.builder()
                .name(name)
                .build();

        employeeRepository.save(employee);

        return employee;
    }

}