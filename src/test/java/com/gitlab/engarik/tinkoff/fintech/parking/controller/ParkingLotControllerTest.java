package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingLotRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.support.TestPropertySourceUtils;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Arrays;
import java.util.UUID;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@WithMockUser(roles = "ADMIN")
@Testcontainers
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ContextConfiguration(initializers = ParkingLotControllerTest.class)
class ParkingLotControllerTest implements ApplicationContextInitializer<ConfigurableApplicationContext> {

    @Autowired
    ParkingLotRepository parkingLotRepository;

    @Autowired
    ParkingRepository parkingRepository;

    @Autowired
    ObjectMapper jackson;

    @Autowired
    private MockMvc mvc;

    @Value("${custom.base-url}/parking-lots")
    private String URI;

    private final UUID testParkingId = UUID.randomUUID();

    @Container
    private static final PostgreSQLContainer<?> postgres = new PostgreSQLContainer<>("postgres:13.1-alpine")
            .withDatabaseName("tinkoff-fintech-parking-test")
            .withUsername("admin")
            .withPassword("admin");

    static {
        postgres.start();
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        TestPropertySourceUtils.addInlinedPropertiesToEnvironment(
                applicationContext,
                "spring.datasource.url=" + postgres.getJdbcUrl(),
                "spring.datasource.username=" + postgres.getUsername(),
                "spring.datasource.password=" + postgres.getPassword()
        );
    }

    @BeforeEach
    public void setUp() {
        parkingRepository.save(
                Parking.builder()
                        .parkingId(testParkingId)
                        .address("Saint-Petersburg")
                        .build());
    }

    @AfterEach
    public void clear() {
        parkingRepository.deleteAll();
        parkingLotRepository.deleteAll();
    }

    @Test
    void saveSuccess() throws Exception {
        ParkingLot parkingLot = createDefaultParkingLot();

        mvc.perform(
                        post(URI)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(jackson.writeValueAsString(parkingLot)))
                .andExpect(status().isCreated());

    }

    @Test
    void findSuccess() throws Exception {
        ParkingLot parkingLot = createParkingLotAndSaveToDatabase("30");

        mvc.perform(get(URI + "/{parkingLotId}", parkingLot.getParkingLotId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parkingLot)));

    }

    @Test
    void findIdNotFound() throws Exception {
        mvc.perform(get(URI + "/{parkingLotId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    @Test
    void findAll() throws Exception {
        ParkingLot parkingLot1 = createParkingLotAndSaveToDatabase("30");
        ParkingLot parkingLot2 = createParkingLotAndSaveToDatabase("31");

        mvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(Arrays.asList(parkingLot1, parkingLot2))));

    }

    @Test
    void updateSuccess() throws Exception {
        ParkingLot parkingLot = createParkingLotAndSaveToDatabase("30");

        mvc.perform(get(URI + "/{parkingLotId}", parkingLot.getParkingLotId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parkingLot)));

        parkingLot.setPosition("239");

        mvc.perform(
                        put(URI + "/{parkingLotId}", parkingLot.getParkingLotId())
                                .content(jackson.writeValueAsString(parkingLot))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        mvc.perform(get(URI + "/{parkingLotId}", parkingLot.getParkingLotId()))
                .andExpect(status().isOk())
                .andExpect(content().json(jackson.writeValueAsString(parkingLot)));
    }

    @Test
    void updateIdNotFound() throws Exception {
        ParkingLot parkingLot = createDefaultParkingLot();

        mvc.perform(
                        put(URI + "/{parkingLotId}", parkingLot.getParkingLotId())
                                .content(jackson.writeValueAsString(parkingLot))
                                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteSuccess() throws Exception {
        ParkingLot parkingLot = createParkingLotAndSaveToDatabase("344");

        mvc.perform(delete(URI + "/{parkingLotId}", parkingLot.getParkingLotId()))
                .andExpect(status().isOk());

    }

    @Test
    void deleteIdNotFound() throws Exception {
        mvc.perform(delete(URI + "/{parkingLotId}", UUID.randomUUID()))
                .andExpect(status().isNotFound());
    }

    private ParkingLot createDefaultParkingLot() {
        return ParkingLot.builder()
                .parkingId(testParkingId)
                .position("1")
                .build();
    }

    private ParkingLot createParkingLotAndSaveToDatabase(String position) {
        ParkingLot parkingLot = ParkingLot.builder()
                .parkingId(testParkingId)
                .position(position)
                .build();

        parkingLotRepository.save(parkingLot);

        return parkingLot;
    }

}