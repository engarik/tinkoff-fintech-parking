package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface CarRepository {

    void save(Car car);

    Car find(UUID carId);

    List<Car> findAll();

    Integer contains(UUID carId);

    Integer update(Car car);

    Integer delete(UUID carId);

    void deleteAll();

}