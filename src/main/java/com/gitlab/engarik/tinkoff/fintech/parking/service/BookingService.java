package com.gitlab.engarik.tinkoff.fintech.parking.service;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Booking;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.BookingRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.CarIsAlreadyParkedException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.CarRegistryIsNotFound;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.IntegrityViolationException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.ParkingLotIsOccupiedException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.BookingMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.BookingRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRegistryRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.EmployeeRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingLotRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class BookingService {

    private final BookingRepository bookingRepository;
    private final BookingMapper bookingMapper;
    private final EmployeeRepository employeeRepository;
    private final CarRepository carRepository;
    private final CarRegistryRepository carRegistryRepository;
    private final ParkingLotRepository parkingLotRepository;

    public Booking save(BookingRequest bookingRequest) {
        if (employeeRepository.contains(bookingRequest.getEmployeeId()) == 0) {
            throw new IntegrityViolationException(Employee.class, bookingRequest.getEmployeeId());
        }
        if (carRepository.contains(bookingRequest.getCarId()) == 0) {
            throw new IntegrityViolationException(Car.class, bookingRequest.getCarId());
        }
        if (carRegistryRepository.contains(bookingRequest.getEmployeeId(), bookingRequest.getCarId()) == 0) {
            throw new CarRegistryIsNotFound(bookingRequest.getEmployeeId(), bookingRequest.getCarId());
        }
        if (parkingLotRepository.contains(bookingRequest.getParkingLotId()) == 0) {
            throw new IntegrityViolationException(ParkingLot.class, bookingRequest.getParkingLotId());
        }

        Booking booking = bookingMapper.toEntity(bookingRequest);

        limitBookingToOneDay(booking);
        checkForTimeIntersection(booking);

        bookingRepository.save(booking);

        return booking;
    }

    public Booking find(UUID bookingId) {
        Booking booking = bookingRepository.find(bookingId);

        if (isNull(booking)) {
            throw new EntityNotFoundException(Booking.class);
        }

        return booking;
    }

    public List<Booking> findAll() {
        return bookingRepository.findAll();
    }

    public Boolean contains(UUID bookingId) {
        return bookingRepository.contains(bookingId) > 0;
    }

    public void update(UUID bookingId, BookingRequest bookingRequest) {
        Booking booking = bookingMapper.toEntityWithId(bookingId, bookingRequest);

        if (bookingRepository.update(booking) == 0) {
            throw new EntityNotFoundException(Booking.class);
        }
    }

    public void delete(UUID bookingId) {
        if (bookingRepository.delete(bookingId) == 0) {
            throw new EntityNotFoundException(Booking.class);
        }
    }

    private void limitBookingToOneDay(Booking booking) {
        if (booking.getStartTime().getDayOfWeek() != booking.getEndTime().getDayOfWeek()) {
            booking.setEndTime(LocalDateTime.of(booking.getStartTime().toLocalDate(), LocalTime.of(23, 59)));
        }
    }

    private void checkForTimeIntersection(Booking booking) {
        checkCarIsNotParkedAtSelectedTime(booking);
        checkParkingLotIsNotOccupiedAtSelectedTime(booking);
    }

    private void checkCarIsNotParkedAtSelectedTime(Booking booking) {
        List<Booking> listOfBookingsToCheck = bookingRepository.getAllBookingsByCarId(booking.getCarId());
        findTimeIntersection(booking, listOfBookingsToCheck)
                .ifPresent((intersection) -> {
                    throw new CarIsAlreadyParkedException(intersection.getStartTime(), intersection.getEndTime());
                });
    }

    private void checkParkingLotIsNotOccupiedAtSelectedTime(Booking booking) {
        List<Booking> listOfBookingsToCheck = bookingRepository.getAllBookingsByParkingLotId(booking.getParkingLotId());
        findTimeIntersection(booking, listOfBookingsToCheck)
                .ifPresent((intersection) -> {
                    throw new ParkingLotIsOccupiedException(intersection.getStartTime(), intersection.getEndTime());
                });
    }

    private Optional<Booking> findTimeIntersection(Booking booking, List<Booking> listOfBookingsToCheck) {
        for (Booking toCheck : listOfBookingsToCheck) {
            if (intersects(booking.getStartTime(), booking.getEndTime(), toCheck.getStartTime(), toCheck.getEndTime())) {
                return Optional.of(toCheck);
            }
        }

        return Optional.empty();
    }

    private boolean intersects(LocalDateTime startTime1, LocalDateTime endTime1, LocalDateTime startTime2, LocalDateTime endTime2) {
        return !(startTime1.isBefore(startTime2) && endTime1.isBefore(startTime2) || startTime1.isAfter(endTime2));
    }

//    @Scheduled(cron = "${custom.clear-timing}")
//    private void clearDatabase() {
//        bookingRepository.deleteBookingsOlderThanADay();
//    }

}
