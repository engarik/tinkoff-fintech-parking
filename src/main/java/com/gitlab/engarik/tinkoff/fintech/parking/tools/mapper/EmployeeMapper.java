package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.EmployeeRequest;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class EmployeeMapper implements EntityMapper<Employee, EmployeeRequest> {

    @Override
    public Employee toEntity(EmployeeRequest employeeRequest) {
        return Employee.builder()
                .name(employeeRequest.getName())
                .build();
    }

    @Override
    public Employee toEntityWithId(UUID id, EmployeeRequest employeeRequest) {
        return Employee.builder()
                .employeeId(id)
                .name(employeeRequest.getName())
                .build();
    }

    @Override
    public EmployeeRequest toRequest(Employee employee) {
        return EmployeeRequest.builder()
                .name(employee.getName())
                .build();
    }

}
