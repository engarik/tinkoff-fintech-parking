package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.service.EmployeeService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.EmployeeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/employees")
@RequiredArgsConstructor
public class EmployeeController {

    private final EmployeeService employeeService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee save(@RequestBody @Valid EmployeeRequest employeeRequest) {
        return employeeService.save(employeeRequest);
    }

    @GetMapping("/{employeeId}")
    public Employee find(@PathVariable UUID employeeId) {
        return employeeService.find(employeeId);
    }

    @GetMapping
    public List<Employee> findAll() {
        return employeeService.findAll();
    }

    @PutMapping("/{employeeId}")
    public void update(@PathVariable UUID employeeId, @RequestBody @Valid EmployeeRequest employeeRequest) {
        employeeService.update(employeeId, employeeRequest);
    }

    @DeleteMapping("/{employeeId}")
    public void delete(@PathVariable UUID employeeId) {
        employeeService.delete(employeeId);
    }

}
