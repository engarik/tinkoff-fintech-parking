package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import com.gitlab.engarik.tinkoff.fintech.parking.service.ParkingLotService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingLotRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/parking-lots")
@RequiredArgsConstructor
public class ParkingLotController {

    private final ParkingLotService parkingLotService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ParkingLot save(@RequestBody @Valid ParkingLotRequest parkingLotRequest) {
        return parkingLotService.save(parkingLotRequest);
    }

    @GetMapping("/{parkingLotId}")
    public ParkingLot find(@PathVariable UUID parkingLotId) {
        return parkingLotService.find(parkingLotId);
    }

    @GetMapping
    public List<ParkingLot> findAll() {
        return parkingLotService.findAll();
    }

    @PutMapping("/{parkingLotId}")
    public void update(@PathVariable UUID parkingLotId, @RequestBody @Valid ParkingLotRequest parkingLotRequest) {
        parkingLotService.update(parkingLotId, parkingLotRequest);
    }

    @DeleteMapping("/{parkingLotId}")
    public void delete(@PathVariable UUID parkingLotId) {
        parkingLotService.delete(parkingLotId);
    }

}
