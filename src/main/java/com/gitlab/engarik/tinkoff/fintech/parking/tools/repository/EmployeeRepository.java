package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface EmployeeRepository {

    void save(Employee employee);

    Employee find(UUID employeeId);

    List<Employee> findAll();

    Integer contains(UUID employeeId);

    Integer update(Employee employee);

    Integer delete(UUID employeeId);

    void deleteAll();

}
