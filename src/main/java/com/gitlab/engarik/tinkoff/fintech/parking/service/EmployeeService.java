package com.gitlab.engarik.tinkoff.fintech.parking.service;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.EmployeeRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.EmployeeMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class EmployeeService {

    private final EmployeeRepository employeeRepository;
    private final EmployeeMapper employeeMapper;

    public Employee save(EmployeeRequest employeeRequest) {
        Employee employee = employeeMapper.toEntity(employeeRequest);

        employeeRepository.save(employee);

        return employee;
    }

    public Employee find(UUID employeeId) {
        Employee employee = employeeRepository.find(employeeId);

        if (isNull(employee)) {
            throw new EntityNotFoundException(Employee.class);
        }

        return employee;
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();
    }

    public Boolean contains(UUID employeeId) {
        return employeeRepository.contains(employeeId) > 0;
    }

    public void update(UUID employeeId, EmployeeRequest employeeRequest) {
        Employee employee = employeeMapper.toEntityWithId(employeeId, employeeRequest);

        if (employeeRepository.update(employee) == 0) {
            throw new EntityNotFoundException(Employee.class);
        }
    }

    public void delete(UUID employeeId) {
        if (employeeRepository.delete(employeeId) == 0) {
            throw new EntityNotFoundException(Employee.class);
        }
    }

}
