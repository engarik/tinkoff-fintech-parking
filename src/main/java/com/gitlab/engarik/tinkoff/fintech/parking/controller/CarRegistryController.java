package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.CarRegistry;
import com.gitlab.engarik.tinkoff.fintech.parking.service.CarRegistryService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRegistryRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/cars-registry")
@RequiredArgsConstructor
public class CarRegistryController {

    private final CarRegistryService carRegistryService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void save(@RequestBody @Valid CarRegistryRequest carRegistryRequest) {
        carRegistryService.save(carRegistryRequest);
    }

    @GetMapping(path = "/{employeeId}/{carId}")
    public Boolean contains(@PathVariable UUID employeeId, @PathVariable UUID carId) {
        return carRegistryService.contains(employeeId, carId);
    }

    @GetMapping
    public List<CarRegistry> findAll() {
        return carRegistryService.findAll();
    }

    @DeleteMapping
    public void delete(@RequestBody @Valid CarRegistryRequest carRegistryRequest) {
        carRegistryService.delete(carRegistryRequest);
    }

}
