package com.gitlab.engarik.tinkoff.fintech.parking.service;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingLotRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.IntegrityViolationException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.ParkingLotMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingLotRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class ParkingLotService {

    private final ParkingLotRepository parkingLotRepository;
    private final ParkingLotMapper parkingLotMapper;
    private final ParkingRepository parkingRepository;

    public ParkingLot save(ParkingLotRequest parkingLotRequest) {
        if (parkingRepository.contains(parkingLotRequest.getParkingId()) == 0) {
            throw new IntegrityViolationException(Parking.class, parkingLotRequest.getParkingId());
        }

        ParkingLot parkingLot = parkingLotMapper.toEntity(parkingLotRequest);

        parkingLotRepository.save(parkingLot);

        return parkingLot;
    }

    public ParkingLot find(UUID parkingLotId) {
        ParkingLot parkingLot = parkingLotRepository.find(parkingLotId);

        if (isNull(parkingLot)) {
            throw new EntityNotFoundException(ParkingLot.class);
        }

        return parkingLot;
    }

    public List<ParkingLot> findAll() {
        return parkingLotRepository.findAll();
    }

    public Boolean contains(UUID parkingLotId) {
        return parkingLotRepository.contains(parkingLotId) > 0;
    }

    public void update(UUID parkingLotId, ParkingLotRequest parkingLotRequest) {
        ParkingLot parkingLot = parkingLotMapper.toEntityWithId(parkingLotId, parkingLotRequest);

        if (parkingLotRepository.update(parkingLot) == 0) {
            throw new EntityNotFoundException(ParkingLot.class);
        }
    }

    public void delete(UUID parkingLotId) {
        if (parkingLotRepository.delete(parkingLotId) == 0) {
            throw new EntityNotFoundException(ParkingLot.class);
        }
    }

}
