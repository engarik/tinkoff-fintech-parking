package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.service.CarService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/cars")
@RequiredArgsConstructor
public class CarController {

    private final CarService carService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Car save(@RequestBody @Valid CarRequest carRequest) {
        return carService.save(carRequest);
    }

    @GetMapping("/{carId}")
    public Car find(@PathVariable UUID carId) {
        return carService.find(carId);
    }

    @GetMapping
    public List<Car> findAll() {
        return carService.findAll();
    }

    @PutMapping("/{carId}")
    public void update(@PathVariable UUID carId, @RequestBody @Valid CarRequest carRequest) {
        carService.update(carId, carRequest);
    }

    @DeleteMapping("/{carId}")
    public void delete(@PathVariable UUID carId) {
        carService.delete(carId);
    }

}
