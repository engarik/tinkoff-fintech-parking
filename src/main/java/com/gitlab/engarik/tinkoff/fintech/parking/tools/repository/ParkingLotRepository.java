package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface ParkingLotRepository {
    
    void save(ParkingLot parkingLot);

    ParkingLot find(UUID parkingLotId);

    List<ParkingLot> findAll();

    Integer contains(UUID parkingLotId);

    Integer update(ParkingLot parkingLot);

    Integer delete(UUID parkingLotId);

    void deleteAll();
    
}
