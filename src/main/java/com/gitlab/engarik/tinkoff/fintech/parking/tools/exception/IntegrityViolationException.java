package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import lombok.Getter;

import java.util.UUID;

@Getter
public class IntegrityViolationException extends RuntimeException {

    UUID id;
    String className;

    public <T> IntegrityViolationException(Class<T> clazz, UUID id) {
        super("Database integrity violation. No such entity");
        this.id = id;
        this.className = clazz.getSimpleName();
    }

}
