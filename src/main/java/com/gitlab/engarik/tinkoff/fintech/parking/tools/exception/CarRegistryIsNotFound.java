package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import lombok.Getter;

import java.util.UUID;

@Getter
public class CarRegistryIsNotFound extends RuntimeException {

    UUID employeeId;
    UUID carId;

    public CarRegistryIsNotFound(UUID employeeId, UUID carId) {
        super("Car is not registried to this employee");
        this.employeeId = employeeId;
        this.carId = carId;
    }

}
