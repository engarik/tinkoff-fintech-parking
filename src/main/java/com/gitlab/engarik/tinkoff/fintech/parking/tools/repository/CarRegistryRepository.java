package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.CarRegistry;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface CarRegistryRepository {

    void save(UUID employeeId, UUID carId);

    Integer contains(UUID employeeId, UUID carId);

    Integer delete(UUID employeeId, UUID carId);

    List<CarRegistry> findByEmployeeId(UUID employeeId);

    List<CarRegistry> findByCarId(UUID carId);

    List<CarRegistry> findAll();

    void deleteAll();

}
