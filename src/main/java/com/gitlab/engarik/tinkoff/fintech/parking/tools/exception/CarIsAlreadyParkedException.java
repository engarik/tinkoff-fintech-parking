package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import java.time.LocalDateTime;

public class CarIsAlreadyParkedException extends IntersectionException {

    private static final String TEMPLATE = "Car is already parked in selected time: %s %s";

    public CarIsAlreadyParkedException(LocalDateTime intersectionStartTime, LocalDateTime intersectionEndTime) {
        super(TEMPLATE, intersectionStartTime, intersectionEndTime);
    }

}
