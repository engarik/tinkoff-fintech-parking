package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import java.time.LocalDateTime;

public abstract class IntersectionException extends RuntimeException {

    public IntersectionException(String toFormat, LocalDateTime intersectionStartTime, LocalDateTime intersectionEndTime) {
        super(String.format(toFormat, intersectionStartTime.toString(), intersectionEndTime.toString()));
    }

}
