package com.gitlab.engarik.tinkoff.fintech.parking.model;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Booking {

    @Builder.Default
    UUID bookingId = UUID.randomUUID();
    LocalDateTime startTime;
    LocalDateTime endTime;
    UUID employeeId;
    UUID carId;
    UUID parkingLotId;

}
