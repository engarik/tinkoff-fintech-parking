package com.gitlab.engarik.tinkoff.fintech.parking.tools.validation;

import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.BookingRequest;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.time.DayOfWeek;
import java.time.temporal.ChronoUnit;

public class BookingValidator implements ConstraintValidator<ValidBooking, BookingRequest> {

    private final int SECONDS_IN_DAY = 86400;

    @Override
    public boolean isValid(BookingRequest bookingRequest, ConstraintValidatorContext constraintValidatorContext) {
        return startTimeIsBeforeEndTime(bookingRequest) && notOnDayOff(bookingRequest) && lessThan24Hours(bookingRequest);
    }

    private boolean startTimeIsBeforeEndTime(BookingRequest bookingRequest) {
        return bookingRequest.getStartTime().isBefore(bookingRequest.getEndTime());
    }

    private boolean lessThan24Hours(BookingRequest bookingRequest) {
        return ChronoUnit.SECONDS.between(bookingRequest.getStartTime(), bookingRequest.getEndTime()) <= SECONDS_IN_DAY;
    }

    private boolean notOnDayOff(BookingRequest bookingRequest) {
        return checkForWorkingDay(bookingRequest.getStartTime().getDayOfWeek()) && checkForWorkingDay(bookingRequest.getEndTime().getDayOfWeek());
    }

    private boolean checkForWorkingDay(DayOfWeek dayOfWeek) {
        return dayOfWeek != DayOfWeek.SATURDAY && dayOfWeek != DayOfWeek.SUNDAY;
    }




}
