package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import com.gitlab.engarik.tinkoff.fintech.parking.model.ParkingLot;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingLotRequest;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ParkingLotMapper implements EntityMapper<ParkingLot, ParkingLotRequest> {

    @Override
    public ParkingLot toEntity(ParkingLotRequest parkingLotRequest) {
        return ParkingLot.builder()
                .parkingId(parkingLotRequest.getParkingId())
                .position(parkingLotRequest.getPosition())
                .build();
    }

    @Override
    public ParkingLot toEntityWithId(UUID id, ParkingLotRequest parkingLotRequest) {
        return ParkingLot.builder()
                .parkingLotId(id)
                .parkingId(parkingLotRequest.getParkingId())
                .position(parkingLotRequest.getPosition())
                .build();
    }

    @Override
    public ParkingLotRequest toRequest(ParkingLot parkingLot) {
        return ParkingLotRequest.builder()
                .parkingId(parkingLot.getParkingId())
                .position(parkingLot.getPosition())
                .build();
    }

}
