package com.gitlab.engarik.tinkoff.fintech.parking;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
@ConfigurationPropertiesScan("com.gitlab.engarik.tinkoff.fintech.parking.tools.security")
public class TinkoffFintechParkingApplication {

    public static void main(String[] args) {
        SpringApplication.run(TinkoffFintechParkingApplication.class, args);
    }

}
