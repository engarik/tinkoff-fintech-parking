package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Booking;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.BookingRequest;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class BookingMapper implements EntityMapper<Booking, BookingRequest> {

    @Override
    public Booking toEntity(BookingRequest bookingRequest) {
        return Booking.builder()
                .startTime(bookingRequest.getStartTime())
                .endTime(bookingRequest.getEndTime())
                .employeeId(bookingRequest.getEmployeeId())
                .carId(bookingRequest.getCarId())
                .parkingLotId(bookingRequest.getParkingLotId())
                .build();
    }

    @Override
    public Booking toEntityWithId(UUID id, BookingRequest bookingRequest) {
        return Booking.builder()
                .bookingId(id)
                .startTime(bookingRequest.getStartTime())
                .endTime(bookingRequest.getEndTime())
                .employeeId(bookingRequest.getEmployeeId())
                .carId(bookingRequest.getCarId())
                .parkingLotId(bookingRequest.getParkingLotId())
                .build();
    }

    @Override
    public BookingRequest toRequest(Booking booking) {
        return BookingRequest.builder()
                .startTime(booking.getStartTime())
                .endTime(booking.getEndTime())
                .employeeId(booking.getEmployeeId())
                .carId(booking.getCarId())
                .parkingLotId(booking.getParkingLotId())
                .build();
    }

}
