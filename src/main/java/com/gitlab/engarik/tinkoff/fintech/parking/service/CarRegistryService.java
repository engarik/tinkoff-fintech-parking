package com.gitlab.engarik.tinkoff.fintech.parking.service;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.model.CarRegistry;
import com.gitlab.engarik.tinkoff.fintech.parking.model.Employee;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRegistryRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.IntegrityViolationException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRegistryRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRepository;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class CarRegistryService {

    private final CarRegistryRepository carRegistryRepository;
    private final EmployeeRepository employeeRepository;
    private final CarRepository carRepository;

    public void save(CarRegistryRequest carRegistryRequest) {
        if (employeeRepository.contains(carRegistryRequest.getEmployeeId()) == 0) {
            throw new IntegrityViolationException(Employee.class, carRegistryRequest.getEmployeeId());
        }
        if (carRepository.contains(carRegistryRequest.getCarId()) == 0) {
            throw new IntegrityViolationException(Car.class, carRegistryRequest.getCarId());
        }

        carRegistryRepository.save(carRegistryRequest.getEmployeeId(), carRegistryRequest.getCarId());
    }

    public Boolean contains(UUID employeeId, UUID carId) {
        return carRegistryRepository.contains(employeeId, carId) > 0;
    }

    public void delete(CarRegistryRequest carRegistryRequest) {
        if (carRegistryRepository.delete(carRegistryRequest.getEmployeeId(), carRegistryRequest.getCarId()) == 0) {
            throw new EntityNotFoundException(CarRegistryRequest.class);
        }
    }

    public List<CarRegistry> findByEmployeeId(UUID employeeId) {
        return carRegistryRepository.findByEmployeeId(employeeId);
    }

    public List<CarRegistry> findByCarId(UUID carId) {
        return carRegistryRepository.findByCarId(carId);
    }

    public List<CarRegistry> findAll() {
        return carRegistryRepository.findAll();
    }

}
