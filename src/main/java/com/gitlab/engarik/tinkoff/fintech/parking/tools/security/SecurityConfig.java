package com.gitlab.engarik.tinkoff.fintech.parking.tools.security;

import lombok.AllArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final UsersCredentials usersCredentials;
    private final PasswordEncoder passwordEncoder;
    @Value("${custom.base-url}")
    private String URI;

    public SecurityConfig(UsersCredentials usersCredentials, PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
        this.usersCredentials = usersCredentials;
    }

    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .csrf().disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.GET).hasAnyRole("ADMIN", "USER")
                .antMatchers(
                        HttpMethod.POST,
                        URI + "/employees/**",
                        URI + "/cars/**",
                        URI + "/cars-registry/**",
                        URI + "/parking/**",
                        URI + "/parking-lots/**").hasRole("ADMIN")
                .antMatchers(HttpMethod.POST, URI + "/booking/**").hasAnyRole("ADMIN", "USER")
                .antMatchers(HttpMethod.DELETE).hasRole("ADMIN")
                .antMatchers(HttpMethod.PUT).hasRole("ADMIN")
                .and()
                .httpBasic();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder authentication) throws Exception
    {
        authentication.inMemoryAuthentication()
                .withUser(usersCredentials.getAdminLogin())
                .password(passwordEncoder.encode(usersCredentials.getAdminPassword()))
                .roles("ADMIN", "USER")
                .and()
                .withUser(usersCredentials.getUserLogin())
                .password(passwordEncoder.encode(usersCredentials.getUserPassword()))
                .roles("USER");
    }

}
