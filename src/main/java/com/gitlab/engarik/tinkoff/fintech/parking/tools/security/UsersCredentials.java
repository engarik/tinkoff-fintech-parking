package com.gitlab.engarik.tinkoff.fintech.parking.tools.security;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Getter
@Setter
@Configuration
@ConfigurationProperties(prefix = "custom.security")
public class UsersCredentials {

    private String adminLogin;
    private String adminPassword;
    private String userLogin;
    private String userPassword;

}
