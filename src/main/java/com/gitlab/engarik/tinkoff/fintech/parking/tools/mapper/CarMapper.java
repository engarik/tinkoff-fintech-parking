package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRequest;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class CarMapper implements EntityMapper<Car, CarRequest> {

    @Override
    public Car toEntity(CarRequest carRequest) {
        return Car.builder()
                .model(carRequest.getModel())
                .registrationPlate(carRequest.getRegistrationPlate())
                .build();
    }

    @Override
    public Car toEntityWithId(UUID id, CarRequest carRequest) {
        return Car.builder()
                .carId(id)
                .model(carRequest.getModel())
                .registrationPlate(carRequest.getRegistrationPlate())
                .build();
    }

    @Override
    public CarRequest toRequest(Car car) {
        return CarRequest.builder()
                .model(car.getModel())
                .registrationPlate(car.getRegistrationPlate())
                .build();
    }

}
