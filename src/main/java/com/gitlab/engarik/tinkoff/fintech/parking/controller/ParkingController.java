package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.service.ParkingService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/parking")
@RequiredArgsConstructor
public class ParkingController {

    private final ParkingService parkingService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Parking save(@RequestBody @Valid ParkingRequest parkingRequest) {
        return parkingService.save(parkingRequest);
    }

    @GetMapping("/{parkingId}")
    public Parking find(@PathVariable UUID parkingId) {
        return parkingService.find(parkingId);
    }
    
    @GetMapping
    public List<Parking> findAll() {
        return parkingService.findAll();
    }

    @PutMapping("/{parkingId}")
    public void update(@PathVariable UUID parkingId, @RequestBody @Valid ParkingRequest parkingRequest) {
        parkingService.update(parkingId, parkingRequest);
    }

    @DeleteMapping("/{parkingId}")
    public void delete(@PathVariable UUID parkingId) {
        parkingService.delete(parkingId);
    }

}
