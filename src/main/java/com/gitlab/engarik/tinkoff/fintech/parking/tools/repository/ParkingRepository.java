package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface ParkingRepository {

    void save(Parking parking);

    Parking find(UUID parkingId);

    List<Parking> findAll();

    Integer contains(UUID parkingId);

    Integer update(Parking parking);

    Integer delete(UUID parkingId);

    void deleteAll();
    
}
