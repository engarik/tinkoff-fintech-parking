package com.gitlab.engarik.tinkoff.fintech.parking.service;


import com.gitlab.engarik.tinkoff.fintech.parking.model.Car;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.CarRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.CarMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.CarRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class CarService {

    private final CarRepository carRepository;
    private final CarMapper carMapper;

    public Car save(CarRequest carRequest) {
        Car car = carMapper.toEntity(carRequest);

        carRepository.save(car);

        return car;
    }

    public Car find(UUID carId) {
        Car car = carRepository.find(carId);

        if (isNull(car)) {
            throw new EntityNotFoundException(Car.class);
        }

        return car;
    }

    public List<Car> findAll() {
        return carRepository.findAll();
    }

    public Boolean contains(UUID carId) {
        return carRepository.contains(carId) > 0;
    }

    public void update(UUID carId, CarRequest carRequest) {
        Car car = carMapper.toEntityWithId(carId, carRequest);

        if (carRepository.update(car) == 0) {
            throw new EntityNotFoundException(Car.class);
        }
    }

    public void delete(UUID carId) {
        if (carRepository.delete(carId) == 0) {
            throw new EntityNotFoundException(Car.class);
        }
    }

}
