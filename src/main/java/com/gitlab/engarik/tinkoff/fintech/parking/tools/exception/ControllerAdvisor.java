package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor {

    @ExceptionHandler(EntityNotFoundException.class)
    public ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException e) {
        return new ResponseEntity<>(Map.of("exception", e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(CarIsAlreadyParkedException.class)
    public ResponseEntity<Object> handleCarIsAlreadyParkedException(CarIsAlreadyParkedException e) {
        return new ResponseEntity<>(Map.of("exception", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(ParkingLotIsOccupiedException.class)
    public ResponseEntity<Object> handleParkingLotIsOccupiedException(ParkingLotIsOccupiedException e) {
        return new ResponseEntity<>(Map.of("exception", e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IntegrityViolationException.class)
    public ResponseEntity<Object> handleIntegrityViolationException(IntegrityViolationException e) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("exception", e.getMessage());
        map.put("class", e.getClassName());
        map.put("id", e.getId().toString());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(CarRegistryIsNotFound.class)
    public ResponseEntity<Object> handleCarRegistryIsNotFoundException(CarRegistryIsNotFound e) {
        Map<String, String> map = new LinkedHashMap<>();
        map.put("exception", e.getMessage());
        map.put("employeeId", e.getEmployeeId().toString());
        map.put("carId", e.getCarId().toString());
        return new ResponseEntity<>(map, HttpStatus.BAD_REQUEST);
    }

}
