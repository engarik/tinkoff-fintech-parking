package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import java.util.UUID;

public interface EntityMapper <K, V> {

    K toEntity(V v);

    K toEntityWithId(UUID id, V v);

    V toRequest(K k);

}
