package com.gitlab.engarik.tinkoff.fintech.parking.tools.repository;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Booking;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.UUID;

@Mapper
public interface BookingRepository {

    void save(Booking booking);

    Booking find(UUID bookingId);

    List<Booking> findAll();

    Integer contains(UUID bookingId);

    Integer update(Booking booking);

    Integer delete(UUID bookingId);

    void deleteAll();

    List<Booking> getAllBookingsByEmployeeId(UUID bookingId);

    List<Booking> getAllBookingsByParkingLotId(UUID parkingLotId);

    List<Booking> getAllBookingsByCarId(UUID carId);

    void deleteBookingsOlderThanADay();

}
