package com.gitlab.engarik.tinkoff.fintech.parking.controller;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Booking;
import com.gitlab.engarik.tinkoff.fintech.parking.service.BookingService;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.BookingRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("${custom.base-url}/booking")
@RequiredArgsConstructor
@Validated
public class BookingController {

    private final BookingService bookingService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Booking save(@RequestBody @Valid BookingRequest bookingRequest) {
        return bookingService.save(bookingRequest);
    }

    @GetMapping("/{bookingId}")
    public Booking find(@PathVariable UUID bookingId) {
        return bookingService.find(bookingId);
    }

    @GetMapping
    public List<Booking> findAll() {
        return bookingService.findAll();
    }

    @PutMapping("/{bookingId}")
    public void update(@PathVariable UUID bookingId, @RequestBody @Valid BookingRequest bookingRequest) {
        bookingService.update(bookingId, bookingRequest);
    }

    @DeleteMapping("/{bookingId}")
    public void delete(@PathVariable UUID bookingId) {
        bookingService.delete(bookingId);
    }

}
