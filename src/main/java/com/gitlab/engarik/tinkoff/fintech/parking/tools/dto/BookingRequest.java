package com.gitlab.engarik.tinkoff.fintech.parking.tools.dto;

import com.gitlab.engarik.tinkoff.fintech.parking.tools.validation.ValidBooking;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@ValidBooking
public class BookingRequest {

    @NotNull
    LocalDateTime startTime;

    @NotNull
    LocalDateTime endTime;

    @NotNull
    UUID employeeId;

    @NotNull
    UUID carId;

    @NotNull
    UUID parkingLotId;

}
