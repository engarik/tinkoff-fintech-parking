package com.gitlab.engarik.tinkoff.fintech.parking.tools.exception;

import java.time.LocalDateTime;

public class ParkingLotIsOccupiedException extends IntersectionException {

    private static final String TEMPLATE = "Selected parking time intersects with existing booking for this parking lot: %s %s";

    public ParkingLotIsOccupiedException(LocalDateTime intersectionStartTime, LocalDateTime intersectionEndTime) {
        super(TEMPLATE, intersectionStartTime, intersectionEndTime);
    }

}
