package com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingRequest;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class ParkingMapper implements EntityMapper<Parking, ParkingRequest> {

    @Override
    public Parking toEntity(ParkingRequest parkingRequest) {
        return Parking.builder()
                .address(parkingRequest.getAddress())
                .build();
    }

    @Override
    public Parking toEntityWithId(UUID id, ParkingRequest parkingRequest) {
        return Parking.builder()
                .parkingId(id)
                .address(parkingRequest.getAddress())
                .build();
    }

    @Override
    public ParkingRequest toRequest(Parking parking) {
        return ParkingRequest.builder()
                .address(parking.getAddress())
                .build();
    }

}
