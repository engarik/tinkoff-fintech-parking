package com.gitlab.engarik.tinkoff.fintech.parking.service;

import com.gitlab.engarik.tinkoff.fintech.parking.model.Parking;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.dto.ParkingRequest;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.exception.EntityNotFoundException;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.ParkingLotMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.mapper.ParkingMapper;
import com.gitlab.engarik.tinkoff.fintech.parking.tools.repository.ParkingRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

import static java.util.Objects.isNull;

@Service
@RequiredArgsConstructor
public class ParkingService {

    private final ParkingRepository parkingRepository;
    private final ParkingMapper parkingMapper;

    public Parking save(ParkingRequest parkingRequest) {
        Parking parking = parkingMapper.toEntity(parkingRequest);

        parkingRepository.save(parking);

        return parking;
    }

    public Parking find(UUID parkingId) {
        Parking parking = parkingRepository.find(parkingId);

        if (isNull(parking)) {
            throw new EntityNotFoundException(Parking.class);
        }

        return parking;
    }

    public Boolean contains(UUID parkingId) {
        return parkingRepository.contains(parkingId) > 0;
    }

    public List<Parking> findAll() {
        return parkingRepository.findAll();
    }

    public void update(UUID parkingId, ParkingRequest parkingRequest) {
        Parking parking = parkingMapper.toEntityWithId(parkingId, parkingRequest);

        if (parkingRepository.update(parking) == 0) {
            throw new EntityNotFoundException(Parking.class);
        }
    }

    public void delete(UUID parkingId) {
        if (parkingRepository.delete(parkingId) == 0) {
            throw new EntityNotFoundException(Parking.class);
        }
    }
}
