CREATE TABLE employees
(
    employee_id UUID PRIMARY KEY UNIQUE,
    name VARCHAR(128) NOT NULL
);

CREATE TABLE cars
(
    car_id UUID PRIMARY KEY UNIQUE,
    model VARCHAR(64) NOT NULL,
    reg_plate VARCHAR(32) NOT NULL
);

CREATE TABLE cars_registry
(
    employee_id UUID,
    car_id UUID,
    UNIQUE (employee_id, car_id),
    PRIMARY KEY (employee_id, car_id),
    FOREIGN KEY(employee_id) REFERENCES employees (employee_id) ON DELETE CASCADE,
    FOREIGN KEY(car_id) REFERENCES cars (car_id) ON DELETE CASCADE
);

CREATE TABLE parkings
(
    parking_id UUID PRIMARY KEY UNIQUE,
    address VARCHAR(256) NOT NULL
);

CREATE TABLE parking_lots
(
    parking_lot_id UUID UNIQUE,
    parking_id UUID,
    position VARCHAR(64) NOT NULL,
    PRIMARY KEY (parking_lot_id, parking_id),
    FOREIGN KEY(parking_id) REFERENCES parkings (parking_id) ON DELETE CASCADE
);

CREATE TABLE bookings
(
    booking_id UUID PRIMARY KEY UNIQUE,
    start_time TIMESTAMP NOT NULL,
    end_time TIMESTAMP NOT NULL,
    employee_id UUID NOT NULL,
    car_id UUID NOT NULL,
    parking_lot_id UUID NOT NULL,
    UNIQUE (employee_id, car_id, parking_lot_id),
    FOREIGN KEY(employee_id, car_id) REFERENCES cars_registry (employee_id, car_id) ON DELETE CASCADE,
    FOREIGN KEY(parking_lot_id) REFERENCES parking_lots (parking_lot_id) ON DELETE CASCADE
);