# Tinkoff Fintech Parking

## Description

Coursework on Java Backend Course by Tinkoff Fintech.
The project is a RESTful web-server that implements parking booking system for Tinkoff employees.

### Database design

![Database design](https://i.imgur.com/3N7cCxf.png)

### Used technologies

* Java 17
* Spring Boot, Spring Security
* PostgreSQL, Mybatis, Flyway
* JUnit, Mockito, Testcontainers
* Maven, Lombok
* Docker, Gitlab CI, Heroku

### How to run

Configure database connection in `application.yml` and then run in docker.

### Author

Nikita Kornilov
[GitHub](https://github.com/engarik)
[Telegram](https://t.me/engarik/)
